<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodolistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todolists', function (Blueprint $table) {    //1st arg = table_name      2nd_arg = blueprint
            $table->increments('id');   //creates a PK column named 'id' which is auto-increment
            $table->timestamps();       //creates created_ad and updated_at column
            $table->string('name');         //create 'name' as string
            $table->text('description');    //create 'description' as text
            // $table->string('city', 100)->after('street');    will create a 'city' column after the 'street' column
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('todolists', function (Blueprint $table) {
            //

        });

        Schema::drop('todolists');
    }
}
