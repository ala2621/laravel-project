@extends('layouts.master')

@section('content')

<h2>Create a New List</h2>

<ul>
	@foreach( $errors->all() as $error )
		<li>{{$error}}</li>
	@endforeach
</ul>

{!! Form::open(array('route' => 'todolists.store', 'class' => 'form')) !!}

<div class="form-group">
	{!! Form::label('List Name') !!}
	{!! Form::text('name', null, 
		array('required', 'class' => 'form-control',
		'placeholder' => 'Enter a list name' )) !!}
</div>
<br>
<div class="form-group">
	{!! Form::label('List Description') !!}
	{!! Form::text('description', null, 
		array('required', 'class' => 'form-control',
		'placeholder' => 'Enter a List Description' )) !!}
</div>	

<h3>Categories</h3>

<div class="form-group">
	{!! Form::label('Categories') !!}
	{!! Form::select('categories[]', $categories, null,
		array('multiple' => 'multiple', 'name' => 'categories[]')) !!}
</div>

<div class="form-group">
	{!! Form::submit('Create List', array('class' => 'btn btn-primary' )) !!}
</div>

{!! Form::close() !!}

@stop