@extends('layouts.master')

@section('content')

<h3>Create a New Task for {{$list->name}}</h3>

<ul>
	@foreach( $errors->all() as $error )
		<li>{{$error}}</li>
	@endforeach
</ul>

{!! Form::open(array('route' => 'tasks.store', 'class' => 'form')) !!}

<div class="form-group">
	{!! Form::label('Task Name') !!}
	{!! Form::text('name', null, 
		array('required', 'class' => 'form-control',
		'placeholder' => 'Enter a task name' )) !!}
	<input type="hidden" name="hidden" value="hidden valus">
</div>

<div class="form-group">
	{!! Form::label('Task Description') !!}
	{!! Form::text('description', null, 
		array('required', 'class' => 'form-control',
		'placeholder' => 'Enter a task description' )) !!}
</div>	

<div class="form-group">
	{!! Form::submit('Create List', array('class' => 'btn btn-primary' )) !!}
</div>



{!! Form::close() !!}


@stop