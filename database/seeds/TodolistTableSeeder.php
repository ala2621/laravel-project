<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use todoparrot\Todolist;

class TodolistTableSeeder extends Seeder
{
	public function run()
	{
		//USING FAKER
		$faker = \Faker\Factory::create();

		Todolist::truncate();	//removes all record in Todolists table

		foreach( range(1, 50) as $index)
		{
			Todolist::create([
				'name' => $faker->sentence(2),
				'description' => $faker->sentence(4)
			]);
		}


		// Todolist::create([
		// 	'name' => 'San Juan Vacation',
		// 	'description' => 'Things to do before we leave for Puerto Rico!'
		// ]);

		// Todolist::create([
		// 	'name' => 'Home Winterization',
		// 	'description' => 'Winter is coming.'
		// ]);

		// Todolist::create([
		// 	'name' => 'Rental Maintenance',
		// 	'description' => 'Cleanup and improvements for new tenants'
		// ]);
	}
}


?>