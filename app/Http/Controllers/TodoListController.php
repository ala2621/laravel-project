<?php

namespace todoparrot\Http\Controllers;

use Illuminate\Http\Request;

use todoparrot\Http\Requests;
use todoparrot\Http\Requests\ListFormRequest;
use todoparrot\Http\Controllers\Controller;

use todoparrot\Todolist;
use todoparrot\Task;
use todoparrot\Category;

use DB;
/*
    you must import the namespace\Todolist for the MODEL
*/

class TodoListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function __construct()
    {
        $this->middleware('iplogger');
    }

    public function index()
    {
        // $items = array('items' => ['Pack luggage', 'Go to airport', 'Arrive in San Juan']);
        // $x = 1 / 0;
        //dd($items);       //it is like var_dump but graphically
        // \Log::debug($items); //sends debug info sa laravel.log
        // \Log::info('Just an informational message.');
        // \Log::warning('Something may be going wrong.');
        // \Log::error('Something is definitely going wrong.');
        // \Log::critical('Danger, Will Robinson! Danger!');
        // \Debugbar::error('Something is definitely going wrong.');
        // echo "Hello.";


        ###PASSSING A VARIABLE TO A VIEW:
        //method1
            // return view('welcome2')->with('name', 'San Juan Vacation');
         //method2
         //another approach: USING MAGIC METHOD withName()
             // $name2 = 'San Juan';
             // return view('welcome2')->withName($name2);

        ###PASSING MULTIPLE VARIABLES TO A VIEW:        
        //METHOD_1
        //NOTE: with() is used
            // $data = array('name' =>  'Lawrence', 
            //               'date' =>   date('Y-m-d'));
            // return view('welcome2') -> with($data);          

        // METHOD_2
        // $name = 'Lawrence';
        // $date = date('Y-m-d');
        // $lists = array('Camping Trip');
        // return view('welcome2', compact('name', 'date', 'lists'));
        
        //---------------------------------------------------------------------------------------------------------------------------------------------


        ##########
        #DATABASE#
        ##########

        // $lists = Todolist::all();                                    //retrieves all records , add ->count() to get the count
        // $lists = Todolist::select('name', 'description')->first();   //retrieves the first record
        // $count = Todolist::count();                                  //gets total rows in table (but it can be directly used in the view)

        //SORTING
        // $lists = Todolist::orderBy('name')->get();                   //with sorting ASC by name
        // $lists = Todolist::orderBy('name', 'DESC')->get();           //with sorting DESC by name

        //sorting using multiple columns
        // $lists = Todolist::orderBy('created_at', 'DESC')
        //     ->orderBy('name', 'ASC')
        //     ->get();
        //end

        //USING CONDITIONAL CLAUSES
        // $lists = Todolist::where('id', '=', '49')->get();   //if equal operator, parameter can be ('id', '50')       //this is not secure
        $completed_tasks = Task::where('done', true)-> get();

        //more secure against SQL injection
        // $lists = Todolist::whereRaw('id = ?', [1])->get();


        //GROUP BY 
        // SELECT YEAR(created_at) AS 'year', COUNT(name) AS 'count' FROM todolists GROUP BY 'name';
        //in laravel, this can be done by
        //to use DB, import DB, (use DB; )

        //DB::raw() allows usage of aggregate function in the query

        // $lists = Todolist::select(
        //          DB::raw('year(created_at) as year'), 
        //          DB::raw('count(name) as `count`'))
        //          ->groupBy('year')
        //          ->orderBy('count', 'desc')->get();

        // $lists = Todolist::select(
        //          DB::raw('year(created_at) as year'), 
        //          DB::raw('count(name) as count'))
        //          ->groupBy('year')
        //          ->where('year', '>', '2010')->get();

        // $lists = Todolist::select(
        //          DB::raw('year(created_at) as year'), 
        //          DB::raw('count(name) as count'))
        //          ->groupBy('year')
        //          ->where('year', '>', '2010')->get();

        //GROUP BY WITH HAVING
        // $lists = Todolist::select(
        // DB::raw('year(created_at) as year'),
        // DB::raw('count(name) as count'))
        // ->groupBy('year')
        // ->having('year', '>', '2010')->get();

        // $lists = Todolist::take(5)->orderBy('created_at', 'desc')->get();   //this will take the five most recently added list

        // $lists = Todolist::take(5)->skip(5)->orderBy('created_at', 'desc')->get();  //it starts with the fifth record as the offset
        
        #GETTING THE FIRST RECORD
        // $lists = Todolist::orderBy('created_at', 'asc')->first(); //it will return an ARRAY of the first record created

        #GETTING THE  LAST RECORD
        // $lists = Todolist::orderBy('created_at', 'desc')->first(); //it will return an ARRAY of the last record created

        #GETTING A RANDOM RECORD
        // $list = Todolist::all()->random(1);  //LESS EFFICIENT
        // $list = Todolist::orderBy(DB::raw('RAND()'))->first(); //MORE EFFICIENT

        #DETERMINING EXISTENCE OF A RECORD
        // $exists = Todolist::where('name', '=', 'Optio et.')->exists();   //returns a boolean if a record exists

        #PAGINATION
        // $lists = Todolist::orderBy('created_at', 'desc')->paginate(10);
        $lists = DB::table('todolists')->paginate(10);
        

        return view('todolists.index')->with('lists', $lists);

        #USING QUERY BUILDER
        // $lists = DB::table('todolists')->get();
        // $lists = DB::table('todolists')->find(52);
        // $lists = DB::select("SELECT * FROM Todolists");
        // DB::insert('insert into todolists (name, description) values (?, ?)',
        //     array('San Juan Vacation', 'Things to do before vacation'));
        // DB::update('update todolists set completed = 1 where id = ?', array(52));
        // DB::delete('delete from todolists where completed = 1');

        // $lists = DB::statement('drop table todolists');
    }

    public function verify(Request $request)
    {
        if($request->user())
        {
            return \Redirect::route('todolists.create')
        }
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        #NOTE: MASS-ASIGNABILITY,  you must define variable $fillable(WHITELISTED) and $guarded(BLACKLISTED) in the model class, BUT NOT BOTH.

        //TO CREATE A RECORD
        #1st method
        // $list =  new Todolist;
        // $list->name = 'jajaja';
        // $list->description = 'jojojo';
        // $list->save();

        #2nd method
        // $list = Todolist::create(
        //     array(  'name' => 'hahaha',
        //             'description' => 'jojojojo')
        //     );

        #3rd method
        // $list = Todolist::firstOrCreate(array('name' => 'Alim'));   //creates a record if it does not exist yet

        #4th method
        // $list = Todolist::firstOrNew(array('name' => 'Shennile'));   //creates a record by creating a new instance
        // $list->description = 'Too much to do before vacation!';
        // $list->save();

        //UPDATING A RECORD 
        // $list = Todolist::updateOrCreate(
        //     array('name' => 'Lawrence'),    //this is the WHERE clause
        //     array('description' => 'Edited')        // this is the field to be updated

        // );
        $categories = Category::lists('name', 'id')->all();
        return view('todolists.create')->with('categories', $categories);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(ListFormRequest $request)
    {
        $list = new Todolist(array(
            'name'          => $request->get('name'),
            'description'   => $request->get('description')
        ));

        $list->save();


        if( count($request->get('categories') ) > 0 ) {
            $list->categories()->attach($request->get('categories'));
        }

        return \Redirect::route('todolists.create')->with('message', 'Your list has been created!');

        // $name = $request->get('name');
        // $description = $request->get('description');
        // $list = Todolist::create(
        //     array('name' => $name,
        //         'description' => $description)
        //         );  

        // return \Redirect::route('todolists.create')->with('message', 'Your list has been created!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    // public function show($id)
    // {
    //     // $list = Todolist::find($id);     //finds a specific record
    //     $list = Todolist::findOrFail($id);  //this one has error catcher
    //     return view('todolists.show')->with('list', $list);
    // }

    public function show($id)
    {
        $list = Todolist::findOrFail($id);

        // $task = new Task;
        // $task->name = 'Walk the dog';
        // $task->description = 'Walk Burky the Matt';
        // $list->tasks()->save($task);

        // $task = new Task;
        // $task->name = 'Mike tacos for dinner';
        // $task->description = 'Mexican sounds really yummy!';
        // $list->tasks()->save($task);


        // $completed_tasks = Todolist::find(1)->tasks()->where('done', true)->get()    - getting completed tasks

        #USING SESSION
        session(['list_name' => $list->name]);  //storing a data to session
        session(['list_id' => $list->id]);

        return view('todolists.show')->with('list', $list);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        // $id = $request->get('list_id');
        $list = Todolist::findOrFail($id);
        // dd($list);
        return view('todolists.edit')->with(['list' => $list]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(ListFormRequest $request, $id)
    {
        //
         $list = Todolist::findOrFail($id);

        $list->update([
            'name'          => $request->get('name'),
            'description '  => $request->get('description')
            ]);

        return \Redirect::route('todolists.show', array($list->id))
            ->with('message', 'Your list has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function delete($id)
    {
        $list = Todolist::findOrFail($id);
        return view('todolists.delete')->with('list', $list);
    }


    public function destroy($id)
    {
        //
        Todolist::destroy($id);

        return \Redirect::route('todolists.index')
            -> with('message', 'The list has been deleted');
    }

    // public function getTwice($n)
    // {
    //     return $n * 2;
    // }

    public function getTwice(Request $request)
    {
        return $request->session()->get('list_name');
    }

    public function many()  //DEMONSTRATES MANY TO MANY RELATIONSHIP - TODOLIST to CATEGORY
    {
        ### TO CREATE A NEW CATEGORY AND ASSOCIATE TO A LIST
        // $list = Todolist::findOrFail(3);
        // $category = new Category(['name' => 'vacation']);
        // $list->categories()->save($category);

        ### ASSOCIATING AN EXISTING CATEGORY TO A LIST
        // $list = Todolist::find(10);
        // $category = Category::find(1);
        // $list->categories()->save($category);


        ### ANOTHER WAY TO ASSOCIATE
        // $list->categories()->attach($category);
        //or
        // $list->categories()->attach(5); //there is already existing cateogory
        //or MULTIPLE ATTACH
        // $list->categories()->attach([3,4]);

        ### TO DISASSCOCIATE
        // $list->categories()->detach(Category::find(3));
        // //or
        // $list->categories()->detach(3);
        // //or
        // $list->categories()->detach([3,4]);

        ###DETERMINING IF A RELATION EXISTS ALREADY
        // $list = Todolist::find(2);
        // $category = Category::find(1);
        // if($list->categories()->contains($category))
        // {
        //     //error message or logic
        // } else {
        //     $list->categories()->save($category);
        // }

        ###SAVING MULTIPLE RELATIONS AT THE SAME TIME
        // $list = Todolist::find(1);

        // $categories = [
        //     new Category(['name' => 'Vacation']),
        //     new Category(['name' => 'Tropical']),
        //     new Category(['name' => 'Leisure']),
        // ];

        // $list->categories()->saveMany($categories);


    }
    
}
