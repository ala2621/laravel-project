<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">

        <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>

        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

        <!-- {!! HTML::style('path/to/bootstrap.css') !!} -->
        <!-- {!! HTML::script('http://code.jquery.com/jquery-1.11.3.min.js') !!} -->
        <!-- {!! HTML::script('path/to/bootstrap.js') !!} -->



        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Laravel 5</div>
            </div>
        </div>

        <a href="http://www.wjgilmore.com"
            class="btn btn-success">W.J. Gilmore</a>
    </body>
</html>
